# KatScript-VSCode

Syntax highlighting for KatScript in VSCode. `template.json` contains the
template for the TextMate grammar which is used for highlighting. Running
`generateregex.py` will load this template, generate regexes for the various
rules from `finesse`, and write the result to
`syntaxes/katscript.tmLanguage.json`.
